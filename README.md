Alonso Krangle LLP fights for victims' rights. Andres Alonso and David Krangle have spent their entire careers fighting for the rights of injured victims. Whether a victim at the hands of another individuals negligence or by corporate carelessness, they understand what it takes to represent victims during this most difficult time.

The partners at Alonso Krangle LLP, Andres Alonso and David Krangle, personally handle their client matters and get on the phone with their clients. 

While other firms may treat clients as a file number, and never speak with you personally, we talk with our clients regularly. 

At Alonso Krangle, you will receive the personal service you deserve. Our personal injury lawyers in New York handle cases both in New York and New Jersey, including automobile accidents, slip and fall, nursing home abuse and other cases.

Additionally, we fight hard for victims of medical malpractice, dangerous drugs and medical equipment, sexual abuse cases, defective products, sexual harassment and other matters.

Call the New York personal injury lawyers at Alonso Krangle LLP today at 516-350-5555  


Website: https://fightforvictims.com/
